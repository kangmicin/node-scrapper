let scraper = require("./scraper.js")
let jsdom = require("jsdom");
let fs = require("fs");
let { JSDOM } = jsdom;


const low = require('lowdb')
const FileSync = require('lowdb/adapters/FileSync')

const adapter = new FileSync('master.json')
const db = low(adapter)

// Set some defaults (required if your JSON file is empty)
db.defaults({ data: {}, keyword: {}, category: {}, tag: {}})
  .write()

// fs.readdir("./drakor/", function(err, items){
//   items.map((item) => {
function scrapJson(file, cb){
fs.readFile(file, {encoding: "utf8"}, (err, content) => {
  let episode, episodeTag, 
      quality, qualityTag, preview = {},
      linkTag = [], link, dom, master = {};
  if(err) return console.log(err);
  let registerTest = []
  var data = JSON.parse(content)
  let categories = data.categories || [];
  let tags = data.tags || [];
  let name = data.title.rendered || "";

  let date = data.date;
  let modifDate = data.modified;
  let id = data.id
  dom = new JSDOM(data.content.rendered || "")
  master = {
    id: id,
    title: name,
    categories: categories,
    tags: tags,
    created: date,
    update: modifDate
  }
  rootTag = Object.assign({}, master)


  episode = {
    last: null,
    preview: function(){
      return preview
    },
    tag: 'episode',
    child: function(){
      return quality
    },
    empty: function() {
      return false
    },
    add: function(result){
      let parentTag = rootTag
      episode.result = result;
      preview.result = episode;
      parentTag.episode = {}
      parentTag.episode[result] = {}
      episodeTag = parentTag.episode[result];

    },
    // reset: function(){
    //   episode.hasResult = false;
    // },
    filter: (e) => {
      episode.hasResult = true;
      return e[0].split(" ")[1]
    },
    test: (i) => {
      preview.test = episode;
      return /^Episode [0-9]+/.exec(i)
    },
    nextTest: function(){
      return quality;
    },
    report: function (){
      return Object.assign({}, rootTag);
    }
  }

  quality = {
    last: null,
    tag: 'qulity',
    child: function(){
      return link
    },
    preview: function(){
      return preview
    },
    empty: function() {
      return !episode.last
    },
    add: function(result){
      let parentTag = episodeTag || rootTag
      quality.result = result;
      preview.result = quality;
      parentTag.quality = {}; 
      parentTag.quality[result] = {}
      qualityTag = parentTag.quality[result]
    },
    // reset: function () {
    //   link.hasResult = false;
    // },
    filter: (e) => {
      quality.hasResult = true;
      return e[0].split("p")[0]
    },
    test: (i) => {
      preview.test = quality;
      return /^[0-9]+p/.exec(i)
    },
    report: function (){
      return Object.assign({}, rootTag);
    },
    nextTest: function(){
      return link;
    },
  }
  link = {
    tag: 'link',
    preview: function(){
      return preview
    },
    add: function(result){
      let parentTag = qualityTag || episodeTag || rootTag
      link.result = result;
      preview.result = link;
      parentTag.link = linkTag;
      linkTag.push(result);
    },
    // reset: function () {
    //   link.hasResult = false;
    // },
    nextTest: function(){
      return episode;
    },
    empty: function() {
      return !quality.last
    },
    filter: (e) => {
      link.hasResult = true;
      return e
    },
    test: (i) => {
      preview.test = link;
      if(i.includes("asianwiki") || i.includes("naver")){return null}
      return /^http[s]*:/.exec(i) ? i : null;
    },
    report: function (){
      return Object.assign({}, rootTag);
    }
  }

  function clean (){
    // episode.hasResult = false;
    // quality.hasResult = false;
    link.hasResult = false;

    linkTag = []
    
    // rootTag = Object.assign({}, master)
  }

  Object.defineProperty(episode, "LAST_TEST_TAG", {value: link.tag})
  Object.defineProperty(quality, "LAST_TEST_TAG", {value: link.tag})
  Object.defineProperty(link, "LAST_TEST_TAG", {value: link.tag})

  Object.defineProperty(episode, "reset", {value: clean})
  Object.defineProperty(quality, "reset", {value: clean})
  Object.defineProperty(link, "reset", {value: clean})

  registerTest = [episode, quality, link]

  function testIndex (i){
    return registerTest[i]
  }
  Object.defineProperty(episode, "count", {
    get() {return registerTest.length}
  })
  Object.defineProperty(quality, "count", {
    get() {return registerTest.length}
  })
  Object.defineProperty(link, "count", {
    get() {return registerTest.length}
  })
  Object.defineProperty(episode, "testIndex", {value: testIndex})
  Object.defineProperty(quality, "testIndex", {value: testIndex})
  Object.defineProperty(link, "testIndex", {value: testIndex})

  var node = dom.window.document.querySelector('body')
  const iterator = scraper(node, registerTest[0] );
  const keywordTb = db.get("keyword")
  const categoryTb = db.get("category")
  const tagsTb = db.get("tag")
  
  getKeyword(data.slug).forEach(keyword => {
    if (keywordTb.has(keyword).value()) {
      db.get(`keyword.${keyword}`).push(id).write();
    }else{
      db.set(`keyword.${keyword}`, [id]).write();
    }
  });
  categories.forEach(category => {
    if (categoryTb.has(category).value()) {
      db.get(`category.${category}`).push(id).write();
    }else{
      db.set(`category.${category}`, [id]).write();
    }
  });
  tags.forEach(tag => {
    if (tagsTb.has(tag).value()) {
      db.get(`tag.${tag}`).push(id).write();
    }else{
      db.set(`tag.${tag}`, [id]).write();
    }
  });
  
  for (const result of iterator) {
    // console.log(result)
    // let ts = Object.assign({}, result)
    // let keys = []
    let data = result;
    let dataTb = db.get("data")
    let finder = result.id;
    let lastFound = false;
    for (const iterator of getNestedKey(result)) {
      // console.log(iterator)
      let temp = finder;
      temp += `.${iterator}`;
      if(data.hasOwnProperty(iterator)){
        if(dataTb.has(temp).value() && typeof data[iterator] === "object" ){
          lastFound = true;
          finder = temp;
          data = data[iterator];
        }
      }
      if (lastFound && !dataTb.has(temp).value()) {
        finder = temp;
        data = data[iterator];
        break;
      }
    }
    dataTb.set(finder, data).write();

  //   for (const episode in result.episode) {
  //   db
  //   .update(`data.${result.id}.episode.${episode}`, result)
  //   .write()
  // }
  }
  // Add a post
  cb()
})
}
function* getNestedKey(obj){
  if(!(Array.isArray(obj))){
  for (const key in obj) {
    yield key
    if ((typeof obj[key] === "object")) {
      yield* getNestedKey(obj[key])
    }
      // console.log(key)
  }
}
}

var stopword = fs.readFileSync("./stopwords.txt", "utf8").split("\n")
var removeSeq = ["episode", "subtitle", "sub"]
var removeword = ["download"]
function getKeyword(slug){
  var wordlist = slug.split("-");
  var keyword = []
  for (let index = 0; index < wordlist.length; index++) {
    const word = wordlist[index];
    if(removeSeq.includes(word)){
      break;
    }
    if(!removeword.includes(word) && !stopword.includes(word) && isNaN(word)){
      keyword.push(word)
    }
  }
  return keyword;
}
// datax = {
//   "asa": {
//     "mana": {
//       "ka": [],
//       "ma": ""
//     }
//   }
// }
// for (const iterator of getNestedKey(datax)) {
//   console.log(iterator)
// }

// })
// })
const _cliProgress = require('cli-progress');
 
// create a new progress bar instance and use shades_classic theme
const bar1 = new _cliProgress.Bar({}, _cliProgress.Presets.shades_classic);
 
// start the progress bar with a total value of 200 and start value of 0

fs.readdir("./test/", function(err, files){
  bar1.start(files.length, 0);
  count = 0;
  files.forEach((fname, index)  => {
    scrapJson(`./test/${fname}`, function(err){
      count += 1
      bar1.update(count)
      if (files.length == count) {
        bar1.stop();
      }
    });
  });
})