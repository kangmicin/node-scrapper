// scrap all return json
function* runner(element, tester) {
  // preview
  let result;
  // let testTags = []
  // if (!(matchers instanceof Array)) {
  //   // return result of matcher
  //   yield matchers;
  // }else{
  // preview = matchers.slice();
  // check semua matcher
  for (let i = 0; i < tester.count; i++) {   
  // }
  // while (matchers) {
    // tester = matchers.shift()
    let {filter, test} = tester;
    // matchers.push(tester)
    // if (testTags.includes(tag)) {
    //   break
    // }
    // testTags.push(tag)
    let node = String.prototype.trim.call(element.nodeValue || element.href || '')
    result = test(node);
    // cocok dan hilangkan dari fungsi
    if (result){
      result = filter(result)
      // matchers.pop() //remove matcher so not execute on next test
      break;
    }
    tester = tester.testIndex(i);
    // touch same tag break it
  }
  preview = tester.preview()
  lastTest = tester.testIndex(tester.count - 1)
  // nextTest = tester.nextTest();
  if(lastTest.hasResult && (tester.tag !== lastTest.tag)){
    yield tester.report();
    tester.reset();
  }
  if(result){tester.add(result)}
  element = element.firstChild;
  // matchers = matchers.length == 0 || (tester.tag === "link" && tester.hasResult) ? tester.report() : matchers;
  while (element) {
    // next dom element
    yield* runner(element, tester);
    // reset matcher for sibiling;
    // matchers = preview;
    element = element.nextSibling;
  }}
// }
module.exports = runner
